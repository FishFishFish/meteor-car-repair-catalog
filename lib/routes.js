if(Meteor.isClient){
    Accounts.onLogin(function(){
            FlowRouter.go('Cars');
    });
    Accounts.onLogout(function(){
            FlowRouter.go('home');
    });
};

FlowRouter.triggers.enter([function(context, redirect){
    if(!Meteor.userId()) {
        FlowRouter.go('home');
    }
}])
FlowRouter.route('/', {
    name: 'home',
    action() {
        if (Meteor.userId()) {
            FlowRouter.go('Cars');
        }
        BlazeLayout.render('HomeLayout');
    }
});
FlowRouter.route('/cars', {
    name: 'Cars',
    action() {
        BlazeLayout.render('MainLayout', {
            main: 'NewCar'
        })
    }
});


FlowRouter.route('/cars/:id', {
    name: 'cars-id',
    action() {
        BlazeLayout.render('MainLayout', {
            main: 'CarSingle'
        })
    }
});