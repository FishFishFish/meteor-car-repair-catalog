import { Mongo } from 'meteor/mongo';

Cars = new Mongo.Collection('cars'); 
Cars.allow({
    insert: function(userId, doc){
        return !!userId;
    },
    update: function(userId, doc){
        return !!userId;
    },
    remove: function(userId, doc){
        return !!userId;
    }
})

Engine = new SimpleSchema({
    volume:{
        type: String
    },
    fuel:{
        type: String
    }
});
Part = new SimpleSchema({
    type:{
        type: String
    },
    serialNumber:{
        type: String
    },
    price:{
        type: Number
    },
    work:{
        type: Number
    }
})
CarSchema = new SimpleSchema({
    name: {
        type: String,
        label: "Name"
    },
    manufacturer:{
        type: String,
        label: "Manufacturer"
    },
    engine:{
        type: [Engine],
        label: "Engine"
    },
    parts:{
        type: [Part],
        label: "Parts"
    }
});

Meteor.methods({
    deleteCar: function(id){
        Cars.remove(id);
    }
});

Cars.attachSchema( CarSchema );
