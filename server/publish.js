Meteor.publish('Cars', function(){
    return Cars.find()
});
Meteor.publish('singleCar', function(id) {
    check(id, String);
    return Cars.find({_id: id})
});