Template.CarSingle.onCreated(function(){
    var self = this;
    self.autorun(function(){
        var id = FlowRouter.getParam('id');
        self.subscribe('singleCar', id);
    });
});

Template.CarSingle.helpers({
    Car: ()=> {
        var id = FlowRouter.getParam('id');
        return Cars.findOne({_id: id});
    }
});

Template.CarSingle.events({
    'click .button-right' : function(){
        var id = FlowRouter.getParam('id');
        console.log(id);
        Meteor.call('deleteCar', id);
        FlowRouter.go('/');
    }
});