Template.CarsIntro.onCreated(function(){
    var self = this;
    self.autorun(function(){
        self.subscribe('Cars');
    });
});

Template.CarsIntro.helpers({
    Cars: ()=> {
        return Cars.find({});
    }
});

