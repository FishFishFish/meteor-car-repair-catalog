Template.Cars.onCreated(function(){
    var self = this;
    self.autorun(function(){
        self.subscribe('Cars');
    });
});

Template.Cars.helpers({
    Cars: ()=> {
        return Cars.find({});
    }
});

